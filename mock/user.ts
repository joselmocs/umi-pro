import { Request, Response } from 'express';

const userResponse = {
  session: '9c9ff3a38fd7fbaef8d6c087bd6054f3',
  userid: '1',
  username: 'mockup',
  name: 'Mockup',
  email: 'mockup@email.com',
  authority: ['guest'],
};

export default {
  'GET /api/account/current': userResponse,
  'POST /api/account/login': (req: Request, res: Response) => {
    const { password, userName } = req.body;

    const response = Object.assign({}, userResponse);

    if (password === '123' && userName === 'admin') {
      response.authority.push('user');
      res.send(response);
      return;
    }
    if (password === '123' && userName === 'user') {
      response.authority.push('admin');
      res.send(response);
      return;
    }

    res.send({});
  },
};
