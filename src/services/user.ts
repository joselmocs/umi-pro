import request from '@/utils/request';

export async function getCurrent(): Promise<any> {
  return request('/api/account/current');
}
