import { Alert, Checkbox } from 'antd';
import React, { useState } from 'react';
import { connect, Dispatch } from 'umi';
import { StateType } from '@/models/login';
import { LoginParamsType } from '@/services/login';
import { ConnectState } from '@/models/connect';
import LoginFrom from './components/Login';

import styles from './style.less';

const { UserName, Password, Submit } = LoginFrom;
interface LoginProps {
  dispatch: Dispatch;
  loginState: StateType;
  submitting?: boolean;
}

const Login: React.FC<LoginProps> = (props) => {
  const { loginState, submitting } = props;
  const { error } = loginState;
  const [autoLogin, setAutoLogin] = useState(true);

  const handleSubmit = (values: LoginParamsType) => {
    const { dispatch } = props;
    dispatch({
      type: 'login/login',
      payload: { ...values },
    });
  };

  return (
    <div className={styles.main}>
      <LoginFrom onSubmit={handleSubmit}>
        <div>
          {error && !submitting && (
            <Alert
              style={{
                marginBottom: 24,
              }}
              message="wrong credentials"
              type="error"
              showIcon
            />
          )}
        </div>
        <UserName name="username" />
        <Password name="password" />
        <div>
          <Checkbox checked={autoLogin} onChange={(e) => setAutoLogin(e.target.checked)}>
            Lembrar este dispositivo
          </Checkbox>
        </div>
        <Submit loading={submitting}>Entrar</Submit>
      </LoginFrom>
    </div>
  );
};

export default connect(({ login, loading }: ConnectState) => ({
  loginState: login,
  submitting: loading.effects['login/login'],
}))(Login);
