import { LockTwoTone, UserOutlined } from '@ant-design/icons';
import React from 'react';
import styles from './index.less';

export default {
  UserName: {
    props: {
      size: 'large',
      id: 'userName',
      prefix: (
        <UserOutlined
          style={{
            color: '#1890ff',
          }}
          className={styles.prefixIcon}
        />
      ),
      placeholder: 'admin ou user',
    },
    rules: [
      {
        required: true,
        message: 'Por favor informe seu nome de usuário!',
      },
    ],
  },
  Password: {
    props: {
      size: 'large',
      prefix: <LockTwoTone className={styles.prefixIcon} />,
      type: 'password',
      id: 'password',
      placeholder: '123',
    },
    rules: [
      {
        required: true,
        message: 'Por favor informe sua senha!',
      },
    ],
  },
};
