import { Effect, Reducer } from 'umi';
import { getCurrent } from '@/services/user';

export interface User {
  session: string;
  userid: string;
  username: string;
  name: string;
  email: string;
  authority: string[];
}

export interface UserModelState {
  current?: User;
}

export interface UserModelType {
  namespace: 'user';
  state: UserModelState;
  effects: {
    getCurrent: Effect;
  };
  reducers: {
    saveCurrent: Reducer<UserModelState>;
  };
}

const UserModel: UserModelType = {
  namespace: 'user',

  state: {},

  effects: {
    *getCurrent(_, { call, put }) {
      const response = yield call(getCurrent);
      yield put({
        type: 'saveCurrent',
        payload: response,
      });
    },
  },

  reducers: {
    saveCurrent(state, action) {
      return {
        ...state,
        current: action.payload || null,
      };
    },
  },
};

export default UserModel;
