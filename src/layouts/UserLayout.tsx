import { MenuDataItem, getMenuData, getPageTitle } from '@ant-design/pro-layout';
import { CopyrightOutlined } from '@ant-design/icons';
import { Link, ConnectProps } from 'umi';
import React, { Fragment } from 'react';
import { useDocumentTitle } from '@/utils/utils';
import FooterComponent from '@/components/Footer';
import styles from './UserLayout.less';
import { getEnvironment } from '../../config/env';
import { layoutConfig } from '../../config/layout';

export interface UserLayoutProps extends Partial<ConnectProps> {
  breadcrumbNameMap: {
    [path: string]: MenuDataItem;
  };
}

const { title, description } = getEnvironment();

const UserLayout: React.FC<UserLayoutProps> = (props) => {
  const {
    children,
    location = {
      pathname: '',
    },
    route = {
      routes: [],
    },
  } = props;

  // temporary title fix
  const { routes = [] } = route;
  const { breadcrumb } = getMenuData(routes);
  const pageTitle = getPageTitle({
    pathname: location.pathname,
    breadcrumb,
    ...layoutConfig,
  });
  useDocumentTitle(pageTitle);
  // temporary title fix

  return (
    <div className={styles.container}>
      <div className={styles.content}>
        <div className={styles.top}>
          <div className={styles.header}>
            <Link to="/">
              <img alt="logo" className={styles.logo} src="/logo.png" />
              <span className={styles.title}>{title}</span>
            </Link>
          </div>
          <div className={styles.desc}>{description}</div>
        </div>
        {children}
      </div>
      <FooterComponent
        copyright={
          <Fragment>
            <CopyrightOutlined /> VP6 IT Consulting
          </Fragment>
        }
      />
    </div>
  );
};

export default UserLayout;
