import { Tag } from 'antd';
import React from 'react';
import Avatar from './AvatarDropdown';
import styles from './index.less';

export default () => {
  return (
    <div className={styles.right}>
      <Avatar />
      {UMI_ENV && UMI_ENV !== 'prod' && (
        <span>
          <Tag color="orange">{UMI_ENV}</Tag>
        </span>
      )}
    </div>
  );
};
