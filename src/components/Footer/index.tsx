import { Layout } from 'antd';
import React, { CSSProperties } from 'react';
import classNames from 'classnames';

const { Footer } = Layout;

interface FooterLink {
  key?: string;
  title: React.ReactNode;
  href: string;
  blankTarget?: boolean;
}

export interface FooterProps {
  links?: FooterLink[];
  copyright?: React.ReactNode;
  style?: CSSProperties;
  className?: string;
}

const FooterComponent: React.FC<FooterProps> = ({
  links,
  copyright,
  style,
  className,
}: FooterProps) => {
  const clsString = classNames('ant-pro-global-footer', className);

  return (
    <Footer className={clsString} style={{ padding: 0, ...style }}>
      {links && (
        <div className="ant-pro-global-footer-links">
          {links.map((link) => (
            <a
              key={link.key}
              title={link.key}
              target={link.blankTarget ? '_blank' : '_self'}
              href={link.href}
            >
              {link.title}
            </a>
          ))}
        </div>
      )}
      {copyright && <div className="ant-pro-global-footer-copyright">{copyright}</div>}
    </Footer>
  );
};

export default FooterComponent;
