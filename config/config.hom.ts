import { defineConfig } from 'umi';
import { environment } from './env/env.hom';

export default defineConfig({
  theme: environment.theme,
});
