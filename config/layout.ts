import { Settings } from '@ant-design/pro-layout';
import { getEnvironment } from './env';

const { navTheme, title, theme } = getEnvironment();

export const layoutConfig: Settings = {
  navTheme: navTheme,
  layout: 'sidemenu',
  contentWidth: 'Fluid',
  fixedHeader: false,
  fixSiderbar: false,
  menu: {
    locale: false,
  },
  iconfontUrl: '',
  title: title,
  primaryColor: theme['@primary-color'],
};
