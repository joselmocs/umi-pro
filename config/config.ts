import { defineConfig } from 'umi';
import { routeConfig } from './routes';
import { environment } from './env/env.dev';

const { UMI_ENV } = process.env;

export default defineConfig({
  hash: true,
  antd: {},
  dva: {
    hmr: true,
  },
  dynamicImport: {
    loading: '@/components/PageLoading/index',
  },
  targets: {
    ie: 11,
  },
  define: {
    UMI_ENV: UMI_ENV || 'dev',
  },
  routes: routeConfig,
  theme: environment.theme,
  ignoreMomentLocale: true,
  manifest: {
    basePath: '/',
  },
});
