import { IEnvironment } from '.';

export const environment: IEnvironment = {
  title: 'VP6 Hom',
  description: 'hom environment',
  apiUrl: 'http://api-hom.vp6.com.br',
  navTheme: 'dark',
  theme: {
    '@primary-color': '#1890ff',
  },
};
