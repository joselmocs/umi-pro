import { IEnvironment } from '.';

export const environment: IEnvironment = {
  title: 'VP6 Pro',
  description: 'production environment',
  apiUrl: 'http://api.vp6.com.br',
  navTheme: 'dark',
  theme: {
    '@primary-color': '#1890ff',
  },
};
