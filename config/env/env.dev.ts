import { IEnvironment } from '.';

export const environment: IEnvironment = {
  title: 'VP6 Dev',
  description: 'develop environment',
  apiUrl: 'http://localhost:8000',
  navTheme: 'dark',
  theme: {
    '@primary-color': '#1890ff',
  },
};
