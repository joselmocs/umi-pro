import { MenuTheme } from 'antd/es/menu/MenuContext';
import { environment as dev } from './env.dev';
import { environment as hom } from './env.hom';
import { environment as prod } from './env.prod';

const environments = {
  dev: dev,
  hom: hom,
  prod: prod,
};

export interface IEnvironment {
  title: string;
  description: string;
  apiUrl: string;
  navTheme: MenuTheme;
  theme: {
    '@primary-color': string;
    '@link-color'?: string;
    '@font-size-base'?: string;
    '@text-color'?: string;
    '@text-color-secondary'?: string;
  };
}

export function getEnvironment(): IEnvironment {
  return environments[UMI_ENV];
}
