import { IRoute } from '@umijs/types';

export const routeConfig: IRoute[] = [
  {
    path: '/user',
    component: '../layouts/UserLayout',
    routes: [
      {
        path: '/user/login',
        component: './user/login',
      },
    ],
  },
  {
    path: '/',
    component: '../layouts/SecurityLayout',
    routes: [
      {
        path: '/',
        component: '../layouts/BasicLayout',
        authority: ['admin', 'user'],
        routes: [
          {
            path: '/',
            redirect: '/dashboard',
          },
          {
            path: '/dashboard',
            name: 'Dashboard',
            icon: 'AreaChartOutlined',
            component: './dashboard',
          },
          {
            path: '/about',
            name: 'Sobre',
            icon: 'QuestionCircleFilled',
            component: './about',
          },
          {
            component: './404',
          },
        ],
      },
      {
        component: './404',
      },
    ],
  },
];
