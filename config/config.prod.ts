import { defineConfig } from 'umi';
import { environment } from './env/env.prod';

export default defineConfig({
  theme: environment.theme,
});
